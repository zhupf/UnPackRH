//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UnPackRH.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_UNPACKRH_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDC_BUTTON_OPENFILE             1000
#define IDC_EDIT_RHFILE                 1001
#define IDC_BUTTON_OUTDIR               1002
#define IDC_EDIT_UNRH                   1003
#define IDC_BUTTON_DEC                  1004
#define IDC_STATIC_INFO                 1005
#define IDC_BUTTON_ENC                  1006
#define IDC_BUTTON_TBLDIR               1009
#define IDC_EDIT_TBLDIR                 1010
#define IDC_BUTTON_TOTBL                1011
#define IDC_BUTTON_TORH                 1012

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
