
// UnPackRHDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "UnPackRH.h"
#include "UnPackRHDlg.h"
#include "afxdialogex.h"
#include "DirDialog.h"
#include "ZCmnFile.h"
#include <openssl\evp.h>
#include <openssl\aes.h>
#include "libxl.h"
#include "GlobalDefs.h"
#include <atlconv.h>
#include <DbgHelp.h>
#pragma comment(lib,"DbgHelp")
#pragma comment(lib,"atlsd")
using namespace libxl;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


#define ZT2A(x) ((char*)CT2A((LPCTSTR)(x)))
string aes_encrypt(string in, string key, const string& iv);
string aes_decrypt(string in, string key, const string& iv);

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CUnPackRHDlg 对话框



CUnPackRHDlg::CUnPackRHDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CUnPackRHDlg::IDD, pParent)
	, m_SrcDir(_T(""))
	, m_DstDir(_T(""))
	, m_TlbDir(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	// Initialize the library
	LIBXL_DllMain(GetModuleHandle(NULL), DLL_PROCESS_ATTACH, NULL);
}

CUnPackRHDlg::~CUnPackRHDlg()
{
	// Finalize the library
	LIBXL_DllMain(GetModuleHandle(NULL), DLL_PROCESS_DETACH, NULL);
}

void CUnPackRHDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_RHFILE, m_SrcDir);
	DDX_Text(pDX, IDC_EDIT_UNRH, m_DstDir);
	DDX_Text(pDX, IDC_EDIT_TBLDIR, m_TlbDir);
}

BEGIN_MESSAGE_MAP(CUnPackRHDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_OPENFILE, &CUnPackRHDlg::OnBnClickedButtonOpenfile)
	ON_BN_CLICKED(IDC_BUTTON_OUTDIR, &CUnPackRHDlg::OnBnClickedButtonOutdir)
	ON_BN_CLICKED(IDC_BUTTON_DEC, &CUnPackRHDlg::OnBnClickedButtonDec)
	ON_BN_CLICKED(IDC_BUTTON_ENC, &CUnPackRHDlg::OnBnClickedButtonEnc)
	ON_BN_CLICKED(IDC_BUTTON_TBLDIR, &CUnPackRHDlg::OnBnClickedButtonTbldir)
	ON_BN_CLICKED(IDC_BUTTON_TOTBL, &CUnPackRHDlg::OnBnClickedButtonTotbl)
	ON_BN_CLICKED(IDC_BUTTON_TORH, &CUnPackRHDlg::OnBnClickedButtonTorh)
END_MESSAGE_MAP()


// CUnPackRHDlg 消息处理程序

BOOL CUnPackRHDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CUnPackRHDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CUnPackRHDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CUnPackRHDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CUnPackRHDlg::OnBnClickedButtonOpenfile()
{
	UpdateData(TRUE);
	CDirDialog _Dlg(_T("UnPackRHDlg_ChooseSrcDir"),m_SrcDir,_T("选择源目录"),_T("浏览"),false,false,true);
	if (IDOK == _Dlg.DoModal()) {
		m_SrcDir = _Dlg.GetPathname();
		UpdateData(FALSE);
	}
	SetDlgItemText(IDC_STATIC_INFO,_T("准备就绪."));
}

void CUnPackRHDlg::OnBnClickedButtonOutdir()
{
	UpdateData(TRUE);
	CDirDialog _Dlg(_T("UnPackRHDlg_ChooseDstDir"),m_DstDir,_T("选择输出目录"),_T("浏览"),false,false,true);
	if (IDOK == _Dlg.DoModal()) {
		m_DstDir = _Dlg.GetPathname();
		UpdateData(FALSE);
	}
	SetDlgItemText(IDC_STATIC_INFO,_T("准备就绪."));
}

string GetFileNameFromPath(const string& _Path)
{
	int _Pos = -1;
	if ((_Pos = _Path.rfind('\\')) != string::npos) {
		string _FileName = _Path.substr(_Pos+1,_Path.size() - _Pos);
		return _FileName;
	}
	return "";
}

void MkDir(const string& _FileOrDir,bool _IsFile = false)
{
	string _Path = _FileOrDir;
	if (!_IsFile && !_Path.empty() && _Path[_Path.size()-1]!='\\') {
		_Path += "\\";
	}
	::MakeSureDirectoryPathExists(_Path.c_str());
}

void FindAllFile(const string& _RootName,const string& _DirName,const string& _Mark,vector<string>& _FileList)
{
	WIN32_FIND_DATAA _FindData;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	string _FindPath = _DirName.empty()?(_RootName+"\\"+_Mark):(_RootName+"\\"+_DirName+"\\"+_Mark);
	hFind = FindFirstFileA(_FindPath.c_str(), &_FindData);
	if (hFind == INVALID_HANDLE_VALUE)
		return;

	string _FindFileName;
	do {
		_FindFileName = _FindData.cFileName;
		if (_FindData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
			if (_FindFileName=="." || _FindFileName=="..") {
				continue;
			}
			else {
				string _NewDir = _DirName.empty()?_FindFileName:_DirName+"\\"+_FindFileName;
				FindAllFile(_RootName,_NewDir,_Mark,_FileList);
			}
		}
		else {
			string _File = _DirName.empty()?_FindFileName:_DirName+"\\"+_FindFileName;
			_FileList.push_back(_File);
		}
	} while (FindNextFileA(hFind, &_FindData) != 0);
	FindClose(hFind);
}

void CUnPackRHDlg::OnBnClickedButtonDec()
{
	SetDlgItemText(IDC_STATIC_INFO,_T("准备就绪."));
	UpdateData(TRUE);
	if (!PathFileExists(m_SrcDir)) {
		MessageBox(_T("源目录不存在!"),_T("错误"),MB_OK);
		return;
	}
	if (m_DstDir.IsEmpty()) {
		MessageBox(_T("输出目录不能为空!"),_T("错误"),MB_OK);
		return;
	}
	string _SrcDir = ZT2A(m_SrcDir);
	string _DstDir = ZT2A(m_DstDir);
	MkDir(_DstDir);
	vector<string> _FileList;
	FindAllFile(_SrcDir,"","*.rh",_FileList);
	if (_FileList.empty()) {
		MessageBox(_T("源目录是空的!"),_T("错误"),MB_OK);
		return;
	}
	for (INT _Idx = 0; _Idx < _FileList.size(); _Idx++) {
		string _EncFile = _SrcDir + "\\" + _FileList[_Idx];
		string _DecFile = _DstDir + "\\" + _FileList[_Idx];
		MkDir(_DecFile,true);
		AesDec(_EncFile,_DecFile);
	}
	SetDlgItemText(IDC_STATIC_INFO,_T("解密完成!"));
}

void CUnPackRHDlg::OnBnClickedButtonEnc()
{
	SetDlgItemText(IDC_STATIC_INFO,_T("准备就绪."));
	UpdateData(TRUE);
	if (!PathFileExists(m_SrcDir)) {
		MessageBox(_T("源目录不存在!"),_T("错误"),MB_OK);
		return;
	}
	if (m_DstDir.IsEmpty()) {
		MessageBox(_T("输出目录不能为空!"),_T("错误"),MB_OK);
		return;
	}
	string _SrcDir = ZT2A(m_SrcDir);
	string _DstDir = ZT2A(m_DstDir);
	MkDir(_DstDir);
	vector<string> _FileList;
	FindAllFile(_SrcDir,"","*.rh",_FileList);
	if (_FileList.empty()) {
		MessageBox(_T("源目录是空的!"),_T("错误"),MB_OK);
		return;
	}
	for (INT _Idx = 0; _Idx < _FileList.size(); _Idx++) {
		string _DecFile = _SrcDir + "\\" + _FileList[_Idx];
		string _EncFile = _DstDir + "\\" + _FileList[_Idx];
		MkDir(_EncFile,true);
		AesEnc(_DecFile,_EncFile);
	}
	SetDlgItemText(IDC_STATIC_INFO,_T("加密完成!"));

}

BOOL CUnPackRHDlg::AesDec(string _EncFile,string _DecFile)
{
	const char* _KeyStr = "gkw3iurpamv;kj20984;asdkfjat1af";
	DWORD _IV = 0xDB0F4940;
	ZCmnFile _File;
	if (_File.OpenFile(_EncFile)) {
		int _FileLen = _File.ZFileGetSize();
		string _FileBuff;
		_FileBuff.resize(_FileLen);
		if (_File.ReadFile((LPVOID)_FileBuff.c_str(),_FileLen)) {
			string _FileBuffDec = aes_decrypt(_FileBuff,_KeyStr,string((char*)&_IV,4));
			if (_File.OpenFile(_DecFile,true)) {
				_File.WriteFile((LPVOID)_FileBuffDec.c_str(),_FileBuffDec.size());
				return TRUE;
			}
		}
	}
	return FALSE;
}

BOOL CUnPackRHDlg::AesEnc(string _DecFile,string _EncFile)
{
	const char* _KeyStr = "gkw3iurpamv;kj20984;asdkfjat1af";
	DWORD _IV = 0xDB0F4940;
	ZCmnFile _File;
	if (_File.OpenFile(_DecFile)) {
		int _FileLen = _File.ZFileGetSize();
		string _FileBuff;
		_FileBuff.resize(_FileLen);
		if (_File.ReadFile((LPVOID)_FileBuff.c_str(),_FileLen)) {
			string _FileBuffDec = aes_encrypt(_FileBuff,_KeyStr,string((char*)&_IV,4));
			if (_File.OpenFile(_EncFile,true)) {
				_File.WriteFile((LPVOID)_FileBuffDec.c_str(),_FileBuffDec.size());
				return TRUE;
			}
		}
	}
	return FALSE;
}

string aes_encrypt(string in, string key, const string& iv)
{
	unsigned char ivec[16] = {0};
	strncpy((char*)ivec, iv.c_str(), iv.size());

	// Always pad the key to 32 bits.. because we can
	if(key.length() < 32){
		key.append(32 - key.length(), '\0');
	}
	
	// Get some space ready for the output
	int length, finalLength = 0;
	string output;
	output.resize(in.size() + AES_BLOCK_SIZE);
	
	// Encrypt the data
	EVP_CIPHER_CTX ctx;
	EVP_CIPHER_CTX_init(&ctx);
	EVP_EncryptInit_ex(&ctx, EVP_aes_256_ecb(), NULL, (unsigned char*) key.c_str(), ivec);
	EVP_EncryptUpdate(&ctx, (unsigned char *)output.c_str(), &length, (unsigned char*)in.c_str(), in.length());
	finalLength += length;
	EVP_EncryptFinal_ex(&ctx, (unsigned char *)output.c_str() + length, &length);
	finalLength += length;

	// Make the data into a string
	output.resize(finalLength - AES_BLOCK_SIZE);
	// clean up
	EVP_CIPHER_CTX_cleanup(&ctx);
	return output;
}

string aes_decrypt(string in, string key, const string& iv)
{
	unsigned char ivec[16] = {0};
	strncpy((char*)ivec, iv.c_str(), iv.size());

	// Always pad the key to 32 bits.. because we can
	if(key.length() < 32){
		key.append(32 - key.length(), '\0');
	}

	// Create some space for output
	int length, finalLength = 0;
	string output;
	output.resize(in.size() + AES_BLOCK_SIZE);
	in.append(AES_BLOCK_SIZE,'\0');
	
	// Decrypt the string
	EVP_CIPHER_CTX ctx;
	EVP_CIPHER_CTX_init(&ctx);
	EVP_DecryptInit_ex(&ctx, EVP_aes_256_ecb(), NULL, (unsigned char*) key.c_str(), ivec);
	EVP_DecryptUpdate(&ctx, (unsigned char *)output.c_str(), &length, (unsigned char*)in.c_str(), in.length());
	finalLength += length;
	EVP_DecryptFinal_ex(&ctx, (unsigned char *)output.c_str() + length, &length);
	finalLength += length;
	
	// Make the output into a string
	output.resize(finalLength);
	// Clean up
	EVP_CIPHER_CTX_cleanup(&ctx);
	return output;
}

void CUnPackRHDlg::OnBnClickedButtonTbldir()
{
	UpdateData(TRUE);
	CDirDialog _Dlg(_T("UnPackRHDlg_ChooseTblDir"),m_TlbDir,_T("选择表目录"),_T("浏览"),false,false,true);
	if (IDOK == _Dlg.DoModal()) {
		m_TlbDir = _Dlg.GetPathname();
		UpdateData(FALSE);
	}
	SetDlgItemText(IDC_STATIC_INFO,_T("准备就绪."));
}

string StripFileExt(string _FileName)
{
	int _Pos = -1;
	if ((_Pos = _FileName.rfind('.')) != string::npos) {
		_FileName = _FileName.substr(0,_Pos);
	}
	return _FileName;
}

void CUnPackRHDlg::OnBnClickedButtonTotbl()
{
	SetDlgItemText(IDC_STATIC_INFO,_T("准备就绪."));
	UpdateData(TRUE);
	if (!PathFileExists(m_DstDir)) {
		MessageBox(_T("解密的RH文件目录不存在!"),_T("错误"),MB_OK);
		return;
	}
	if (m_TlbDir.IsEmpty()) {
		MessageBox(_T("输出表目录不能为空!"),_T("错误"),MB_OK);
		return;
	}
	string _TlbDir = ZT2A(m_TlbDir);
	string _DstDir = ZT2A(m_DstDir);
	MkDir(_TlbDir);
	vector<string> _FileList;
	FindAllFile(_DstDir,"","*.rh",_FileList);
	if (_FileList.empty()) {
		MessageBox(_T("RH文件目录是空的!"),_T("错误"),MB_OK);
		return;
	}
	for (INT _Idx = 0; _Idx < _FileList.size(); _Idx++) {
		string _EncFile = _DstDir + "\\" + _FileList[_Idx];
		string _DecFile = _TlbDir + "\\" + StripFileExt(_FileList[_Idx]) + ".xls";
		MkDir(_DecFile,true);
		TblDec(_EncFile,_DecFile);
	}
	SetDlgItemText(IDC_STATIC_INFO,_T("解表完成!"));
}

void CUnPackRHDlg::OnBnClickedButtonTorh()
{
	SetDlgItemText(IDC_STATIC_INFO,_T("准备就绪."));
	UpdateData(TRUE);
	if (!PathFileExists(m_TlbDir)) {
		MessageBox(_T("输入表目录不存在!"),_T("错误"),MB_OK);
		return;
	}
	if (m_DstDir.IsEmpty()) {
		MessageBox(_T("解密的RH文件目录不能为空!"),_T("错误"),MB_OK);
		return;
	}
	string _TlbDir = ZT2A(m_TlbDir);
	string _DstDir = ZT2A(m_DstDir);
	MkDir(_DstDir);
	vector<string> _FileList;
	FindAllFile(_TlbDir,"","*.xls",_FileList);
	FindAllFile(_TlbDir,"","*.xlsx",_FileList);
	if (_FileList.empty()) {
		MessageBox(_T("输入表目录是空的!"),_T("错误"),MB_OK);
		return;
	}
	string _FileName;
	for (INT _Idx = 0; _Idx < _FileList.size(); _Idx++) {
		string _DecFile = _TlbDir + "\\" + _FileList[_Idx];
		string _EncFile = _DstDir + "\\" + StripFileExt(_FileList[_Idx]) + ".rh";
		MkDir(_EncFile,true);
		TblEnc(_DecFile,_EncFile);
	}
	SetDlgItemText(IDC_STATIC_INFO,_T("转表完成!"));
}

enum RHTBL_ColType
{
	ColType_INT32,
	ColType_FLOAT32,
	ColType_UNKNOWN,
	ColType_STRING,
	ColType_INT64,
};

unsigned int g_Int32Data;
float g_Float32Data;
wstring g_StringData;
__int64 g_Int64Data;

void CUnPackRHDlg::GetData(INT _Type,BYTE*& _InData)
{
	switch (_Type)
	{
	case ColType_INT32:
		{
			g_Int32Data = *(INT*)_InData;
			_InData+=4;
		}
		break;
	case ColType_FLOAT32:
		{
			g_Float32Data = *(float*)_InData;
			_InData+=4;
		}
		break;
	case ColType_UNKNOWN:
		{
			AfxMessageBox(_T("未知表列类型: 2"));
		}
		break;
	case ColType_STRING:
		{
			static wstring s_TempStr;
			g_StringData.clear();
			s_TempStr = wstring((WCHAR*)(_InData+2),*(WORD*)_InData);
			s_TempStr.append(1,L'\0');
			g_StringData = s_TempStr.c_str();
			_InData+=s_TempStr.size()*2;
		}
		break;
	case ColType_INT64:
		{
			g_Int64Data = *(__int64*)_InData;
			_InData+=8;
		}
		break;
	default:
		{
			AfxMessageBox(_T("未知表列类型: 未知"));
		}
		break;
	}
}

void CUnPackRHDlg::SetData(INT _Type,BYTE*& _InData)
{
	switch (_Type)
	{
	case ColType_INT32:
		{
			g_Int32Data = *(INT*)_InData;
			_InData+=4;
		}
		break;
	case ColType_FLOAT32:
		{
			g_Float32Data = *(float*)_InData;
			_InData+=4;
		}
		break;
	case ColType_UNKNOWN:
		{
			AfxMessageBox(_T("未知表列类型: 2"));
		}
		break;
	case ColType_STRING:
		{
			static wstring s_TempStr;
			g_StringData.clear();
			s_TempStr = wstring((WCHAR*)(_InData+2),*(WORD*)_InData);
			s_TempStr.append(1,L'\0');
			g_StringData = s_TempStr.c_str();
			_InData+=s_TempStr.size()*2;
		}
		break;
	case ColType_INT64:
		{
			g_Int64Data = *(__int64*)_InData;
			_InData+=8;
		}
		break;
	default:
		{
			AfxMessageBox(_T("未知表列类型: 未知"));
		}
		break;
	}
}

BOOL CUnPackRHDlg::TblDec(string _EncFile,string _DecFile)
{
	Book* book;
	Sheet* sheet;
	int _RowNum = 0;
	int _ColNum = 0;
	vector<int> _ColTypeList;
	vector<wstring> _ColNameList;
	ZCmnFile _FileEnc;
	if (_FileEnc.OpenFile(_EncFile) && (book = xlCreateBook()) && (sheet = book->addSheet(_T("Sheet1")))) {
		INT _EncLen = _FileEnc.ZFileGetSize();
		string _EncBuff;_EncBuff.resize(_EncLen);
		_FileEnc.ReadFile((LPVOID)_EncBuff.c_str(),_EncLen);
		BYTE* _BuffPtr = (BYTE*)_EncBuff.c_str();
		BYTE* _BuffStart = _BuffPtr;
		BYTE* _BuffEnd = _BuffPtr + _EncBuff.size();
		_RowNum =  *(int*)_BuffPtr;
		_ColNum =  *(int*)(_BuffPtr+4);
		_BuffPtr += 8;
		while (_BuffPtr < _BuffEnd) {
			while (_ColNameList.size() < _ColNum) {
				GetData(ColType_STRING,_BuffPtr);
				sheet->writeStr(0, _ColNameList.size(), g_StringData.c_str());
				_ColNameList.push_back(g_StringData);
			}
			while (_ColTypeList.size() < _ColNum) {
				GetData(ColType_INT32,_BuffPtr);
				sheet->writeNum(1, _ColTypeList.size(), (int)g_Int32Data);
				_ColTypeList.push_back(g_Int32Data);
			}
			int _RowIdx = 1;
			int _ColIdx = 0;
			int _XlsRows = _RowNum + 2;
			while (++_RowIdx < _XlsRows) {
				for (_ColIdx = 0; _ColIdx < _ColNum; _ColIdx++) {
					switch (_ColTypeList[_ColIdx])
					{
					case ColType_INT32:
						{
							GetData(_ColTypeList[_ColIdx],_BuffPtr);
							sheet->writeNum(_RowIdx,_ColIdx,(int)g_Int32Data);
						}
						break;
					case ColType_FLOAT32:
						{
							GetData(_ColTypeList[_ColIdx],_BuffPtr);
							sheet->writeNum(_RowIdx,_ColIdx,g_Float32Data);
						}
						break;
					case ColType_UNKNOWN:
						{
							AfxMessageBox(_T("未知表列类型: 2"));
						}
						break;
					case ColType_STRING:
						{
							GetData(_ColTypeList[_ColIdx],_BuffPtr);
							sheet->writeStr(_RowIdx,_ColIdx,g_StringData.c_str());
						}
						break;
					case ColType_INT64:
						{
							GetData(_ColTypeList[_ColIdx],_BuffPtr);
							sheet->writeStr(_RowIdx,_ColIdx,CA2T(AllToStr(g_Int64Data).c_str()));
						}
						break;
					default:
						{
							AfxMessageBox(_T("未知表列类型: 未知"));
						}
						break;
					}
				}
			}
			book->save(CA2T(_DecFile.c_str()));
			book->release();
			break;
		}
	}
	return TRUE;
}

BOOL CUnPackRHDlg::TblEnc(string _DecFile,string _EncFile)
{
	Book* book;
	Sheet* sheet;
	int _RowNum = 0;
	int _ColNum = 0;
	vector<int> _ColTypeList;
	vector<wstring> _ColNameList;
	ZCmnFile _FileEnc;
	if ((book = xlCreateBook()) && (book->load(CA2T(_DecFile.c_str()))) && (sheet = book->getSheet(0))) {
		INT _RowIdx = sheet->firstRow();
		INT _RowNum = sheet->lastRow();
		INT _ColIdx = sheet->firstCol();
		INT _ColNum = sheet->lastCol();
		if (_RowNum < 2) {
			book->release();
			return FALSE;
		}
		if (!_FileEnc.OpenFile(_EncFile,true,true)) {
			return FALSE;
		}
		INT _RhRows = _RowNum - 2;
		_FileEnc.WriteFile((LPVOID)&_RhRows,sizeof(_RhRows));
		_FileEnc.WriteFile((LPVOID)&_ColNum,sizeof(_ColNum));
		const TCHAR* _pStr;
		WORD _StrLen;
		while (_ColNameList.size() < _ColNum) {
			g_StringData = (_pStr = sheet->readStr(0,_ColNameList.size()))?_pStr:_T("");
			_StrLen = g_StringData.size();
			_FileEnc.WriteFile((LPVOID)&_StrLen,sizeof(_StrLen));
			_FileEnc.WriteFile((LPVOID)g_StringData.c_str(),_StrLen*2);
			_ColNameList.push_back(g_StringData);
		}
		while (_ColTypeList.size() < _ColNum) {
			g_Int32Data = sheet->readNum(1,_ColTypeList.size());
			_FileEnc.WriteFile((LPVOID)&g_Int32Data,sizeof(g_Int32Data));
			_ColTypeList.push_back(g_Int32Data);
		}
		_RowIdx = 1;
		while (++_RowIdx < _RowNum) {
			for (_ColIdx = 0; _ColIdx < _ColNum; _ColIdx++) {
				switch (_ColTypeList[_ColIdx])
				{
				case ColType_INT32:
					{
						g_Int32Data = sheet->readNum(_RowIdx,_ColIdx);
						_FileEnc.WriteFile((LPVOID)&g_Int32Data,sizeof(g_Int32Data));
					}
					break;
				case ColType_FLOAT32:
					{
						g_Float32Data = sheet->readNum(_RowIdx,_ColIdx);
						_FileEnc.WriteFile((LPVOID)&g_Float32Data,sizeof(g_Float32Data));
					}
					break;
				case ColType_UNKNOWN:
					{
						AfxMessageBox(_T("未知表列类型: 2"));
					}
					break;
				case ColType_STRING:
					{
						g_StringData = (_pStr = sheet->readStr(_RowIdx,_ColIdx))?_pStr:_T("");
						_StrLen = g_StringData.size();
						_FileEnc.WriteFile((LPVOID)&_StrLen,sizeof(_StrLen));
						_FileEnc.WriteFile((LPVOID)g_StringData.c_str(),_StrLen*2);
					}
					break;
				case ColType_INT64:
					{
						g_StringData = (_pStr = sheet->readStr(_RowIdx,_ColIdx))?_pStr:_T("");
						g_Int64Data = StrToAll<__int64>(string(CT2A(g_StringData.c_str())));
						_FileEnc.WriteFile((LPVOID)&g_Int64Data,sizeof(g_Int64Data));
					}
					break;
				default:
					{
						AfxMessageBox(_T("未知表列类型: 未知"));
					}
					break;
				}
			}
		}
		book->release();
		INT _FileLen = _FileEnc.ZFileTell();
		INT _Tail = _FileLen % 16;
		if (_Tail != 0) {
			_Tail = 16 - _Tail - 1;
			string _Padding(1,'*');
			_Padding.append(_Tail,'\0');
			_FileEnc.WriteFile((LPVOID)_Padding.c_str(),_Padding.size());
		}
	}
	return TRUE;
}