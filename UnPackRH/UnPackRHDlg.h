;
// UnPackRHDlg.h : 头文件
//

#pragma once
#include "ZCmnFile.h"
#include <string>
#include <vector>
using std::string;
using std::wstring;
using std::vector;

// CUnPackRHDlg 对话框
class CUnPackRHDlg : public CDialogEx
{
// 构造
public:
	CUnPackRHDlg(CWnd* pParent = NULL);	// 标准构造函数
	~CUnPackRHDlg();

// 对话框数据
	enum { IDD = IDD_UNPACKRH_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持

public:
	BOOL AesDec(string _EncFile,string _DecFile);
	BOOL AesEnc(string _DecFile,string _EncFile);
	BOOL TblDec(string _EncFile,string _DecFile);
	BOOL TblEnc(string _DecFile,string _EncFile);
	void GetData(INT _Type,BYTE*& _InData);
	void SetData(INT _Type,BYTE*& _InData);
private:
	CString m_SrcDir;
	CString m_DstDir;
	CString m_TlbDir;

// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonOpenfile();
	afx_msg void OnBnClickedButtonOutdir();
	afx_msg void OnBnClickedButtonDec();
	afx_msg void OnBnClickedButtonEnc();
	afx_msg void OnBnClickedButtonTbldir();
	afx_msg void OnBnClickedButtonTotbl();
	afx_msg void OnBnClickedButtonTorh();
};
